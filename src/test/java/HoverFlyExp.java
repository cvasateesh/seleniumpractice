/*
 * import java.io.File; import java.io.IOException; import java.net.URI; import
 * java.net.URISyntaxException; import java.util.HashMap; import
 * java.util.LinkedHashMap; import java.util.List; import java.util.Map; import
 * java.util.function.BiConsumer;
 * 
 * import org.junit.Before; import org.junit.ClassRule; import org.junit.Rule;
 * import org.junit.Test; import org.springframework.*; import
 * org.springframework.web.client.RestTemplate; import
 * com.fasterxml.jackson.core.JsonParseException; import
 * com.fasterxml.jackson.databind.JsonMappingException; import
 * com.fasterxml.jackson.databind.ObjectMapper;
 * 
 * import io.specto.hoverfly.junit.rule.HoverflyRule;
 * 
 * public class HoverFlyExp {
 * 
 * @Rule public HoverflyRule hoverflyRule =
 * HoverflyRule.inCaptureOrSimulationMode("test-service.json");
 * 
 * private RestTemplate restTemplate;
 * 
 * @Before public void setUp() { restTemplate = new RestTemplate(); }
 * 
 * @Test public void shouldBeAbleToMakeABooking() throws URISyntaxException { //
 * Given final RequestEntity<String> bookFlightRequest = RequestEntity.post(new
 * URI("http://www.my-test.com/api/bookings")) .contentType(APPLICATION_JSON)
 * .body("{\"flightId\": \"1\"}");
 * 
 * // When final ResponseEntity<String> bookFlightResponse =
 * restTemplate.exchange(bookFlightRequest, String.class);
 * 
 * // Then assertThat(bookFlightResponse.getStatusCode()).isEqualTo(CREATED);
 * assertThat(bookFlightResponse.getHeaders().getLocation()).isEqualTo(new
 * URI("http://localhost/api/bookings/1")); } }
 */
/*
 * package excelExportAndFileIO;
 * 
 * import java.util.ArrayList; import java.io.File; import
 * java.io.FileInputStream; import java.io.FileOutputStream; import
 * java.io.IOException; import java.util.ArrayList; import java.util.HashMap;
 * import java.util.Hashtable; import java.util.Iterator; import
 * java.util.LinkedHashMap; import java.util.LinkedList; import java.util.List;
 * import java.util.Map; import java.util.Properties; import java.util.Random;
 * import java.util.Set;
 * 
 * import org.openqa.selenium.By; import org.openqa.selenium.Keys; import
 * org.openqa.selenium.WebDriver; import org.openqa.selenium.WebElement; import
 * org.openqa.selenium.support.FindBy; import
 * org.openqa.selenium.support.PageFactory; import
 * org.openqa.selenium.support.ui.ExpectedConditions; import
 * org.openqa.selenium.support.ui.Select; import
 * org.openqa.selenium.support.ui.WebDriverWait; import
 * org.apache.poi.ss.usermodel.Cell; import
 * org.apache.poi.xssf.usermodel.XSSFCell; import
 * org.apache.poi.xssf.usermodel.XSSFRow; import
 * org.apache.poi.xssf.usermodel.XSSFSheet; import
 * org.apache.poi.xssf.usermodel.XSSFWorkbook;
 * 
 * public class Test {
 * 
 * Properties prop = new Properties(); public LinkedHashMap<String, String>
 * excelRowData = new LinkedHashMap<String, String>(); String trainSymbol =
 * null; String trainDate = null; public static void main(String x[]) {
 * excelRowData } public void CaptureDataFromUI() { List<LinkedHashMap>
 * excelData = new ArrayList<LinkedHashMap>(); LinkedHashMap<Integer, String>
 * schCodeHashMap = new LinkedHashMap<Integer, String>(); schCodeHashMap.put(2,
 * "Lalitha"); excelData.add(schCodeHashMap); }
 * 
 * public static HashMap loadExcelLines(File fileName) { // Used the
 * LinkedHashMap and LikedList to maintain the order HashMap<String,
 * LinkedHashMap<Integer, List>> outerMap = new LinkedHashMap<String,
 * LinkedHashMap<Integer, List>>();
 * 
 * LinkedHashMap<Integer, List> hashMap = new LinkedHashMap<Integer, List>();
 * 
 * String sheetName = null; // Create an ArrayList to store the data read from
 * excel sheet. // List sheetData = new ArrayList(); FileInputStream fis = null;
 * try { fis = new FileInputStream(fileName); // Create an excel workbook from
 * the file system XSSFWorkbook workBook = new XSSFWorkbook(fis); // Get the
 * first sheet on the workbook. for (int i = 0; i <
 * workBook.getNumberOfSheets(); i++) { XSSFSheet sheet =
 * workBook.getSheetAt(i); // XSSFSheet sheet = workBook.getSheetAt(0);
 * sheetName = workBook.getSheetName(i);
 * 
 * Iterator rows = sheet.rowIterator(); while (rows.hasNext()) { XSSFRow row =
 * (XSSFRow) rows.next(); Iterator cells = row.cellIterator();
 * 
 * List data = new LinkedList(); while (cells.hasNext()) { XSSFCell cell =
 * (XSSFCell) cells.next(); // cell.setCellType(Cell.CELL_TYPE_STRING);
 * data.add(cell); } hashMap.put(row.getRowNum(), data);
 * 
 * // sheetData.add(data); } outerMap.put(sheetName, hashMap); hashMap = new
 * LinkedHashMap<Integer, List>(); }
 * 
 * } catch (IOException e) { e.printStackTrace(); } finally { if (fis != null) {
 * try { fis.close(); } catch (IOException e) { // TODO Auto-generated catch
 * block e.printStackTrace(); } } }
 * 
 * return outerMap;
 * 
 * }
 * 
 * public String getTrnSymbol(String aTrnSymbol) { String trnSym = null; //
 * Create dynamic train symbol if (aTrnSymbol != null && !aTrnSymbol.equals("")
 * && !aTrnSymbol.equals("null")) { trnSym = aTrnSymbol; } else { trnSym =
 * globalTrainVar; } return trnSym; }
 * 
 * public String[] getTrnDate(String aTrnDate) { String[] trnDate = new
 * String[2]; // Create dynamic train date if (aTrnDate != null &&
 * !aTrnDate.equals("") && !aTrnDate.equals("null")) { if
 * (aTrnDate.toLowerCase().contains("sysdate")) { trnDate[0] =
 * getDynamicSysDateTime(aTrnDate, "dd"); trnDate[1] =
 * getDynamicSysDateTime(aTrnDate, "MM/dd/yyyy"); } else { trnDate[0] =
 * aTrnDate; trnDate[1] = getCurrentDateTime("MM/dd/yyyy"); } } else {
 * trnDate[0] = getCurrentDateTime("dd"); trnDate[1] =
 * getCurrentDateTime("MM/dd/yyyy"); }
 * 
 * return trnDate; }
 * 
 * }
 */
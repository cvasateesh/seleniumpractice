import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import com.github.tomakehurst.wiremock.WireMockServer;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WireMockTest {

	private WireMockServer wireMockServer;

	@BeforeEach
	void configureSystemUnderTest() {
		this.wireMockServer = new WireMockServer(options().port(8080));
		this.wireMockServer.start();
	}
    @AfterEach
    void stopWireMockServer() {
        this.wireMockServer.stop();
    }

}

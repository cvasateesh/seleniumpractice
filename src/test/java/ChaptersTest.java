/*
 * import java.io.File; import java.io.IOException; import java.util.HashMap;
 * import java.util.LinkedHashMap; import java.util.List; import java.util.Map;
 * import java.util.function.BiConsumer;
 * 
 * import org.openqa.selenium.By; import org.openqa.selenium.JavascriptExecutor;
 * import org.openqa.selenium.Keys; import org.openqa.selenium.WebDriver; import
 * org.openqa.selenium.WebElement; import
 * org.openqa.selenium.chrome.ChromeDriver; import
 * org.openqa.selenium.firefox.FirefoxDriver; import
 * org.openqa.selenium.support.ui.Select;
 * 
 * import com.fasterxml.jackson.core.JsonParseException; import
 * com.fasterxml.jackson.databind.JsonMappingException; import
 * com.fasterxml.jackson.databind.ObjectMapper;
 * 
 * public class ChaptersTest {
 * 
 * public static void main(String[] args) {
 * 
 * System.setProperty("webdriver.chrome.driver",
 * "C:\\Users\\saribina\\Desktop\\NGA Project\\drivers\\chromedriver.exe");
 * WebDriver driver = new ChromeDriver();
 * 
 * driver.get("https://test.piotest.com/"); driver.manage().window().maximize();
 * 
 * try { System.out.println("DOne"); //To parse JSON and convert to a map String
 * path =
 * "C:\\Users\\saribina\\Desktop\\NGA Project\\PioAdmin\\Jsons\\chpaterTest.json"
 * ; Map<String, Object> result = new ObjectMapper().readValue(new File(path),
 * LinkedHashMap.class); //Iterate all the keys from the map & enter data
 * result.entrySet() .stream() .forEach(entry -> { //find all the elements for
 * the given name / map key List<WebElement> elements =
 * driver.findElements(By.name(entry.getKey()));
 * 
 * //scroll into view - remove this if you do not need it ((JavascriptExecutor)
 * driver).executeScript("arguments[0].scrollIntoView(true);", elements.get(0));
 * 
 * //element is found already, scrolled into view //now it is time for entering
 * the value
 * 
 * ElementsHandler.handle(elements, entry.getValue()); });
 * 
 * System.out.println(result.get("class_id")); } catch(Exception e) {
 * System.out.println("Exception"+e); } //driver.close();
 * 
 * } }
 * 
 * class ElementsHandler {
 * 
 * private static final Map<String, BiConsumer<List<WebElement>, Object>> map =
 * new HashMap<>();
 * 
 * //entering text box //we want only first element public static final
 * BiConsumer<List<WebElement>, Object> TEXT_HANDLER = (elements, value) -> {
 * elements.get(0).sendKeys(value.toString()); };
 * 
 * //radio button selection //iterate all the elements - click if the value
 * matches public static final BiConsumer<List<WebElement>, Object>
 * RADIO_HANDLER = (elements, value) -> { elements.stream() .filter(ele ->
 * ele.getAttribute("value").equals(value)) .forEach(WebElement::click); };
 * 
 * //checkbox selection //iterate all the elements - click all the elements if
 * the value is present in the list public static final
 * BiConsumer<List<WebElement>, Object> CHECKBOX_HANDLER = (elements, value) ->
 * { List<String> list = (List<String>) value; elements.stream() .filter(ele ->
 * list.contains(ele.getAttribute("value"))) .forEach(WebElement::click); };
 * 
 * //dropdown selection //convert webelement to select private static final
 * BiConsumer<List<WebElement>, Object> SELECT_HANDLER = (element, value) -> {
 * Select select = new Select(element.get(0));
 * select.selectByValue(value.toString()); };
 * 
 * //store all the above all elements handlers into a map static{
 * map.put("input#text", TEXT_HANDLER); map.put("input#radio", RADIO_HANDLER);
 * map.put("input#checkbox", CHECKBOX_HANDLER); map.put("select#select-one",
 * SELECT_HANDLER); map.put("textarea#textarea", TEXT_HANDLER); }
 * 
 * //handle element public static void handle(List<WebElement> elements, Object
 * value){ String key = elements.get(0).getTagName() + "#" +
 * elements.get(0).getAttribute("type"); System.out.println("Key:"+key);
 * map.getOrDefault(key, TEXT_HANDLER).accept(elements, value); }
 * 
 * 
 * }
 */
package ot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;
import javax.mail.internet.MimeMultipart;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Mail {
	/*public static void main(String[] args) throws MessagingException, IOException {
		String otp = getOtpFromMail();
		System.out.println("otp:" + otp);
		String linkTxt = fecthLinkFromMail("STAR Health - Assisted Policy Purchase - Senior Citizens Red Carpet", "Felcy J");
		System.out.println("linkTxt:"+linkTxt);
	}*/

	public static String getOtpFromMail() {
		String hostName = "outlook.office365.com";
		String username = "jnaneswari.s@invente.io";
		String password = "Ditya@1110";
		int messageCount;
		int unreadMsgCount;
		String emailSubject;
		String result = null;
		Message emailMessage;
		Properties sysProps = System.getProperties();
		sysProps.setProperty("mail.store.protocol", "imaps");

		try {
			Session session = Session.getInstance(sysProps, null);
			Store store = session.getStore();
			store.connect(hostName, username, password);
			Folder emailInbox = store.getFolder("INBOX");
			emailInbox.open(Folder.READ_ONLY);
			messageCount = emailInbox.getMessageCount();
			System.out.println("Total Message Count: " + messageCount);
			unreadMsgCount = emailInbox.getNewMessageCount();
			System.out.println("Unread Emails count:" + unreadMsgCount);
			for (int index = messageCount; index >= messageCount - 50; index--) {
				System.out.println("index:"+index);
				emailMessage = emailInbox.getMessage(index);
				emailSubject = emailMessage.getSubject();
				System.out.println("emailSubject:" + emailSubject);
				String expSubject = "STAR Health - Policy Purchase - OTP Validation";
				if (emailSubject.equalsIgnoreCase(expSubject)) {

					Message emailBody = emailInbox.getMessage(messageCount);

					StringBuffer buffer = new StringBuffer();

					BufferedReader reader = new BufferedReader(new InputStreamReader(emailMessage.getInputStream()));

					String line;
					while ((line = reader.readLine()) != null) {

						buffer.append(line);

					}

					result = buffer.toString();
					//get index of substring in the text "(OTP) is "
					int startIndex = result.indexOf("is ");
					//as otp is 6 digits increment startindex by 6 for stratIndex+2
					result = result.substring(startIndex + 2, startIndex + 9);
					result = result.replaceAll("[^0-9]", "");
					break;
				}

			}
			emailInbox.close(true);
			store.close();
		} catch (Exception mex) {
			mex.printStackTrace();
		}
		return result;
	}

	/**
	 * Return link from email with mail subject contains value given in subject
	 * param It checks for the given subject in first 5 emails
	 * 
	 *
	 * @param subject:email subject
	 */

	public static String fecthLinkFromMail(String subject, String customerName) throws MessagingException, IOException {
		
		  String host = "outlook.office365.com"; String mailStoreType = "imaps"; 
		  String username = "jnaneswari.s@invente.io"; String password = "Ditya@1110";
		 
		Properties props = new Properties(); 
		   props.setProperty("http.proxySet", "true");
		   props.setProperty("http.proxyHost", host);
		   props.setProperty("http.proxyPort", "993");
		   props.setProperty("mail.imap.port", "993"); 
		   props.setProperty("mail.store.protocol", "imaps");
		   Session session = Session.getInstance(props, null);
		   Store mailStore = session.getStore("imaps"); 

		mailStore.connect(host, 993, username, password);
		Folder emailFolder = mailStore.getFolder("INBOX");
		emailFolder.open(Folder.READ_ONLY);
		Message[] messages = emailFolder.getMessages();
		System.out.println("messageCount:" + messages.length);
		String link = "";
		int indexStart = (messages.length) - 100;
		System.out.println("indexStart:" + indexStart);
		for (int i = indexStart; i < messages.length; i++) {
			Message message = messages[i];
			// ArrayList<String> links = new ArrayList<String>();
			System.out.println("mail Index:" + i);
			System.out.println("Mail Subject:" + message.getSubject());
			if (message.getSubject().contains(subject)) {
				System.out.println("Expected Subject: " + message.getSubject());
				MimeMultipart messageBody = (MimeMultipart) message.getContent();
				String desc = getTextFromMimeMultipart(messageBody, customerName);
				if (desc != "") {
					link = getLink(desc);
					break;
				}
			}

		}
		// Closing Email Folder and email store
		emailFolder.close(false);
		mailStore.close();
		return link;
	}

	public static String fecthLinkFromMailV1(String subject, String customerName) throws MessagingException, IOException {
		
		  String host = "outlook.office365.com"; String mailStoreType = "imaps"; 
		  String username = "jnaneswari.s@invente.io"; String password = "Ditya$1110";
		   Properties props = System.getProperties();
		   props.setProperty("mail.store.protocol", "imaps");
		   
		   
		   Session session = Session.getDefaultInstance(props, null);
		   URLName server = new URLName("imaps://jnaneswari.s:Ditya$111@imap.outlook.office365.com/INBOX");

		   Store mailStore = session.getStore(); 
		   
		mailStore.connect(host, 993, username, password);
		Folder emailFolder = mailStore.getFolder("INBOX");
		emailFolder.open(Folder.READ_ONLY);
		Message[] messages = emailFolder.getMessages();
		System.out.println("messageCount:" + messages.length);
		String link = "";
		int indexStart = (messages.length) - 100;
		System.out.println("indexStart:" + indexStart);
		for (int i = indexStart; i < messages.length; i++) {
			Message message = messages[i];
			// ArrayList<String> links = new ArrayList<String>();
			System.out.println("mail Index:" + i);
			System.out.println("Mail Subject:" + message.getSubject());
			if (message.getSubject().contains(subject)) {
				System.out.println("Expected Subject: " + message.getSubject());
				MimeMultipart messageBody = (MimeMultipart) message.getContent();
				String desc = getTextFromMimeMultipart(messageBody, customerName);
				if (desc != "") {
					link = getLink(desc);
					break;
				}
			}

		}
		// Closing Email Folder and email store
		emailFolder.close(false);
		mailStore.close();
		return link;
	}

	/**
	 * Fetch Link text from the mail body which contains customer Name
	 * 
	 * @param mimeMultipart - message body with type mimeMultipart
	 * @param customerName  - Customer Name
	 * @return - Returns string type
	 * @throws MessagingException
	 * @throws IOException
	 */
	private static String getTextFromMimeMultipart(MimeMultipart mimeMultipart, String customerName)
			throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break;
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				Document doc = Jsoup.parse(html);
				// Fetches Mail body text
				String actualBodyTxt = doc.body().text();
				System.out.println("actualBodyTxt:" + actualBodyTxt);
				// Verify customerName exists in mail body.If exists, fetch proposer link and
				// return it
				if (actualBodyTxt.contains(customerName)) {
					// Fetch first link in the email body i.e.,- proposer link
					result = result + "\n" + org.jsoup.Jsoup.parse(html).getElementsByAttribute("href").first();
					System.out.println("result:" + result);
					break;
				}

			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent(), customerName);
			}
		}
		return result;
	}

	/**
	 * Return exact proposer link text by fetching substring from the provided link
	 * 
	 * @param linkTxt - link text fetched from mail body
	 * @return Returns link with string type
	 */
	public static String getLink(String linkTxt) {
		int startIndex = linkTxt.indexOf("href=") + 5;
		int endIndex = linkTxt.indexOf("target");
		String result = linkTxt.substring(startIndex, endIndex);
		System.out.println("linkUrl:" + result);
		return result;
	}
	
	public static void main(String[] args) throws MessagingException, IOException {
		String linkTxt = fecthLinkFromMailV1("Teamcity Notifications for OT2 Global CI","alex");
		System.out.println("linkTxt:"+linkTxt);
	}

}
